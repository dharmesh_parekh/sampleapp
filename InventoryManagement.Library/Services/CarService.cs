﻿using InventoryManagement.Core.Entities;
using InventoryManagement.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;


namespace InventoryManagement.Library.Services
{
    public interface ICarService
    {
        List<Car> GetAll();
        Car GetById(long Id);
        long Save(Car car);

        void Delete(Car car);

        List<GetCars_Result> GetCarList(int userId, int pageNo, int pageSize, string sortColumn, string sortOrder, string searchValue = "");

    }

    public class CarService : ICarService
    {
        IUnitOfWork<InventoryManagementEntities> _uow;
        public CarService(IUnitOfWork<InventoryManagementEntities> uow)
        {
            _uow = uow;
        }

        public List<Car> GetAll()
        {
            return _uow.Repository<Car>().Table.ToList();
        }

        public Car GetById(long Id)
        {
            return _uow.Repository<Car>().GetById(Id);
        }

        public long Save(Car car)
        {
            if (car.CarId > 0)
                _uow.Repository<Car>().Update(car);
            else
                _uow.Repository<Car>().Insert(car);
            _uow.Commit();
            return car.CarId;
        }

        public void Delete(Car car)
        {
            _uow.Repository<Car>().Delete(car);
            _uow.Commit();
        }

        public List<GetCars_Result> GetCarList(int userId, int pageNo, int pageSize, string sortColumn, string sortOrder, string searchValue = "")
        {
            return _uow.Repository<Car>().Context.Database.SqlQuery<GetCars_Result>("exec GetCars {0},{1},{2},{3},{4},{5}", userId, pageNo, pageSize, sortColumn, sortOrder, searchValue).ToList();
        }

    }
}
