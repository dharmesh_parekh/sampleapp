﻿using InventoryManagement.Core.Entities;
using InventoryManagement.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Library.Services
{
    public interface IUserService
    {
        User ValidateUserLogin(string email, string password);
        List<GetUsers_Result> GetUserList(int pageNo, int pageSize, string sortColumn, string sortOrder, string searchValue = "");
        User GetById(int Id);
        int Save(User user);
        void Delete(User user);

        bool CheckUserExists(string email, int userId);

    }

    public class UserService : IUserService
    {
        IUnitOfWork<InventoryManagementEntities> _uow;

        public UserService(IUnitOfWork<InventoryManagementEntities> uow)
        {
            _uow = uow;
        }

        public User GetById(int Id)
        {
            return _uow.Repository<User>().GetById(Id);
        }


        public bool CheckUserExists(string email, int userId)
        {
            return _uow.Repository<User>().Table.Any(x => x.Email == email && x.UserId != userId);
        }

        public int Save(User user)
        {
            if (user.UserId > 0)
                _uow.Repository<User>().Update(user);
            else
                _uow.Repository<User>().Insert(user);
            _uow.Commit();
            return user.UserId;
        }

        public void Delete(User user)
        {
            _uow.Repository<User>().Delete(user);
            _uow.Commit();
        }

        public User ValidateUserLogin(string email, string password)
        {
            return _uow.Repository<User>().Table.Where(x => x.Email == email && x.Password == password).FirstOrDefault();
        }

        public List<GetUsers_Result> GetUserList(int pageNo, int pageSize, string sortColumn, string sortOrder, string searchValue = "")
        {        
            return _uow.Repository<User>().Context.Database.SqlQuery<GetUsers_Result>("exec GetUsers {0},{1},{2},{3},{4}", pageNo, pageSize, sortColumn, sortOrder, searchValue).ToList();
        }
    }
}
