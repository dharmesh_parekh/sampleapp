﻿using InventoryManagement.Core.Entities;
using InventoryManagement.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Library.Services
{
    public interface IRoleService
    {
        List<Role> GetRoles();
        Role GetById(short roleId);
    }


    public class RoleService : IRoleService
    {
        IUnitOfWork<InventoryManagementEntities> _uow;

        public RoleService(IUnitOfWork<InventoryManagementEntities> uow)
        {
            _uow = uow;
        }

        
        public List<Role> GetRoles()
        {
            return _uow.Repository<Role>().Table.OrderBy(x=>x.Name).ToList();
        }

        public Role GetById(short roleId)
        {
            return _uow.Repository<Role>().GetById(roleId);
        }

    }
}
